# Companion repository for publication with title "Impact of exposure frequency on disease burden of the common cold"

The paper is written in [Quarto](https://quarto.org/), an "open-source scientific and technical publishing system". The calculations in the paper are performed in the language [R](https://www.r-project.org/).

## Required software
To render the documents we recommend to install:

- [R](https://www.r-project.org/)
- [RStudio](https://posit.co/download/rstudio-desktop/) - contains Quarto as well
- Several packages in R 
  - To install them type in R: `install.packages(c('tidyverse', 'patchwork', 'deSolve', 'latex2exp'))`

## How to reproduce the paper
* Checkout this repository via `git` (or download as a zip file from [here](https://gitlab.com/imb-dev/impact-of-exposure-frequency-on-disease-burden-of-the-common-cold/-/archive/main/impact-of-exposure-frequency-on-disease-burden-of-the-common-cold-main.zip) and unzip it)
* Open the file `paper.qmd` in [RStudio](https://posit.co/download/rstudio-desktop/) and hit the button `Render`

## Preprint
A preprint version of the paper has been published at [medRxiv](https://www.medrxiv.org/content/10.1101/2024.04.26.24306416v1). The current version of the paper and the supplement can be found in this repository.


## Info about files in this repository
* `paper.qmd`: Main file to compile the html version of the paper
  * Further needed files to render `paper.qmd` into `paper.html`
    * `graphics/fig-3.jpg`
    * `infections-exposure-symptoms.bib`
* `supplement.qmd`: Supplement of the paper
* `abstract_350_words.txt`: Abstract of the paper
* `paper.R` and `supplement.R`: R code extracted from `paper.qmd` and `supplement.qmd` via [`knitr::purl()`](https://rdrr.io/cran/knitr/man/knit.html)
* `session_info.txt`: Info about the package versions used by the creators of this repository
