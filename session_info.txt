R version 4.3.2 (2023-10-31)
Platform: aarch64-apple-darwin20 (64-bit)
Running under: macOS Sonoma 14.6

Matrix products: default
BLAS:   /Library/Frameworks/R.framework/Versions/4.3-arm64/Resources/lib/libRblas.0.dylib 
LAPACK: /Library/Frameworks/R.framework/Versions/4.3-arm64/Resources/lib/libRlapack.dylib;  LAPACK version 3.11.0

locale:
[1] en_US.UTF-8/en_US.UTF-8/en_US.UTF-8/C/en_US.UTF-8/en_US.UTF-8

time zone: Europe/Berlin
tzcode source: internal

attached base packages:
[1] stats     graphics  grDevices utils     datasets  methods   base     

other attached packages:
 [1] latex2exp_0.9.6 deSolve_1.40    patchwork_1.2.0 lubridate_1.9.3
 [5] forcats_1.0.0   stringr_1.5.1   dplyr_1.1.4     purrr_1.0.2    
 [9] readr_2.1.4     tidyr_1.3.0     tibble_3.2.1    ggplot2_3.5.1  
[13] tidyverse_2.0.0

loaded via a namespace (and not attached):
 [1] utf8_1.2.4        generics_0.1.3    stringi_1.8.2     lattice_0.22-5   
 [5] hms_1.1.3         digest_0.6.34     magrittr_2.0.3    evaluate_0.23    
 [9] grid_4.3.2        timechange_0.2.0  fastmap_1.1.1     Matrix_1.6-4     
[13] jsonlite_1.8.8    mgcv_1.9-0        fansi_1.0.6       scales_1.3.0     
[17] textshaping_0.3.7 cli_3.6.2         rlang_1.1.3       munsell_0.5.0    
[21] splines_4.3.2     withr_3.0.0       yaml_2.3.7        tools_4.3.2      
[25] tzdb_0.4.0        colorspace_2.1-0  vctrs_0.6.5       R6_2.5.1         
[29] lifecycle_1.0.4   htmlwidgets_1.6.4 ragg_1.2.6        pkgconfig_2.0.3  
[33] pillar_1.9.0      gtable_0.3.4      glue_1.7.0        systemfonts_1.0.5
[37] xfun_0.41         tidyselect_1.2.1  rstudioapi_0.15.0 knitr_1.45       
[41] farver_2.1.1      htmltools_0.5.7   nlme_3.1-164      rmarkdown_2.25   
[45] labeling_0.4.3    compiler_4.3.2   
